{
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };
  outputs = {self, nixpkgs, utils}:
    utils.lib.eachDefaultSystem (system:
      let 
        pkgs = nixpkgs.legacyPackages.${system};
      in rec {
        defaultPackage = pkgs.rustPlatform.buildRustPackage {
          pname = "sonic-cli";
          version = "0.1.0";

          src = ./.;

          cargoLock = {
            lockFile = ./Cargo.lock;
          };
        };

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            cargo
            rustc
          ];
        };
        packages.docker = pkgs.dockerTools.buildImage {
          name = "sonic-cli";
          contents = defaultPackage;
          config = {
            Cmd = [ "${defaultPackage}/bin/sonic-cli" ];
          };
        };
      }
    );
}

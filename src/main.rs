use std::net::ToSocketAddrs;

use sonic_channel::{
    IngestChannel,
    SearchChannel,
    SonicChannel, Dest, CountRequest, FlushRequest
};
use clap::Parser;
use color_eyre::eyre;

#[derive(clap::Parser)]
struct Options {
    #[clap(subcommand)]
    command: Command,

    #[clap(long, default_value = "localhost")]
    host: String,

    #[clap(short, long, default_value = "1491")]
    port: u16,

    #[clap(long, default_value = "SecretPassword")]
    password: String,
}

#[derive(clap::Parser)]
enum Command {
    Search(SearchOpt),
    Count(CountOpt),
    Flush(FlushOpt),
}

#[derive(clap::Parser)]
struct SearchOpt {
    #[clap(short, long)]
    query: String,

    #[clap(short, long)]
    collection: String,

    #[clap(short, long, default_value = "default")]
    bucket: String,

    #[clap(short, long)]
    limit: Option<usize>,
}

#[derive(clap::Parser)]
struct CountOpt {
    #[clap(short, long)]
    collection: String,

    #[clap(short, long, default_value = "default")]
    bucket: String,
}

#[derive(clap::Parser)]
struct FlushOpt {
    #[clap(short, long)]
    collection: String,

    #[clap(short, long)]
    bucket: Option<String>,

    #[clap(short, long)]
    id: Option<String>,
}

fn main() -> eyre::Result<()> {
    color_eyre::install()?;

    let options = Options::parse();

    let socket_addr = (options.host.as_str(), options.port);

    match &options.command {
        Command::Search(search_opt) => search(socket_addr, &options, search_opt)?,
        Command::Count(count_opt) => count(socket_addr, &options, count_opt)?,
        Command::Flush(flush_opt) => flush(socket_addr, &options, flush_opt)?,
    };
    
    Ok(())
}

fn search(
    socket_addr: impl ToSocketAddrs,
    options: &Options,
    search_opt: &SearchOpt,
) -> eyre::Result<()> {
    let channel = SearchChannel::start(
        socket_addr,
        &options.password
    )?;
    let objects = channel.query(
        sonic_channel::QueryRequest {
            dest: Dest::col_buc(
                &search_opt.collection,
                &search_opt.bucket
            ),
            terms: search_opt.query.clone(),
            lang: None,
            limit: search_opt.limit,
            offset: None
        }
    )?;

    for obj in objects {
        println!("{}", obj);
    }

    Ok(())
}

fn count(
    socket_addr: impl ToSocketAddrs,
    options: &Options,
    count_opt: &CountOpt,
) -> eyre::Result<()> {
    let channel = IngestChannel::start(
        socket_addr,
        &options.password
    )?;
    let object_count = channel.count(
        CountRequest::objects(
            &count_opt.collection,
            &count_opt.bucket,
        )
    )?;

    println!("{}", object_count);

    Ok(())
}

fn flush(
    socket_addr: impl ToSocketAddrs,
    options: &Options,
    flush_opt: &FlushOpt,
) -> eyre::Result<()> {
    let flush_request = match flush_opt {
        FlushOpt { collection, bucket: None, id: None } => {
            FlushRequest::collection(collection)
        },
        FlushOpt { collection, bucket: Some(bucket), id: None } => {
            FlushRequest::bucket(collection, bucket)
        },
        FlushOpt { collection, bucket: Some(bucket), id: Some(id) } => {
            FlushRequest::object(collection, bucket, id)
        },
        FlushOpt { bucket: None, id: Some(_), .. } => {
            eyre::bail!("If id is defined must also define bucket")
        }
    };

    let channel = IngestChannel::start(
        socket_addr,
        &options.password
    )?;

    channel.flush(flush_request)?;

    Ok(())
}
